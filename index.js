const core = {
  agent: require("./src/agent"),
  fs: require("./src/host/fs"),
  serverInfo: require("./src/host/serverinfo"),
  mem: require("./src/host/mem"),
  cpuload: require("./src/host/cpuload"),
  cpuProcess: require("./src/host/process"),
  networkInterfaces: require("./src/host/network"),
  sleep: require('system-sleep'),
};

async function main() {
  core.agent();
  await core.fs();
  await core.serverInfo();
  await core.mem();
  await core.cpuload();
  await core.cpuProcess();
  await core.networkInterfaces();
}

while (true) {
  main()
  core.sleep(60000)
}