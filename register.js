const fs = require("fs")
const readline = require("readline-sync")

const config_file = "config/cfg.js"

const secret_key = readline.question("Secret Code: ")

registerConfig(secret_key);

function registerConfig(secret_key) {
  const configString = `const cfg = {
  secret_key: "${secret_key}",
  endpoint: "http://sms-analysis.me:8001/ords/horus/v2",
  date_format: "Y/m/d H:M:S"
};

module.exports = cfg;`;

  fs.writeFile(config_file, configString, err => {})
  
}