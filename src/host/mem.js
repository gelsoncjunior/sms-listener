const si = require("systeminformation")
const registerData = require("../../core/registerData")

const category = "memory"

async function memData() {
  const meminfo = await si.mem();
  const data = JSON.stringify(meminfo)
  registerData(category, data)
}

module.exports = memData;
