const si = require("systeminformation");
const registerData = require("../../core/registerData")

const category = "filesystem"

async function diskData() {
  const diskinfo = await si.fsSize();
  const data = JSON.stringify(diskinfo)
  registerData(category, data)
}

module.exports = diskData