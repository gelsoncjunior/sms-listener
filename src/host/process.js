const si = require("systeminformation");
const registerData = require("../../core/registerData")

const category = "process"

async function cpuProcessData() {
  const cpuProcessInfo = await si.processes();
  const data = JSON.stringify(cpuProcessInfo)
  registerData(category, data)
}

module.exports = cpuProcessData;
