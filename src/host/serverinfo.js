const si = require("systeminformation");
const registerData = require("../../core/registerData")

const category = "informationSystem"

async function serverInfoData() {
  const serverInfoSystem = await si.getStaticData();
  const serverInfoUptime = await si.time().uptime;

  out = {
    platform: serverInfoSystem.os.platform,
    distro: serverInfoSystem.os.distro,
    release: serverInfoSystem.os.release,
    kernel: serverInfoSystem.os.kernel,
    arch: serverInfoSystem.os.arch,
    hostname: serverInfoSystem.os.hostname,
    codepage: serverInfoSystem.os.codepage,
    serial: serverInfoSystem.os.serial,
    manufacturer: serverInfoSystem.system.manufacturer,
    uptime: serverInfoUptime
  }

  const data = JSON.stringify(out)
  registerData(category, data)

}

module.exports = serverInfoData;
