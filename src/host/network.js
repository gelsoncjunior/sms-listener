const si = require("systeminformation");
const registerData = require("../../core/registerData")

const category = "network"

async function networkData() {
  const networkInfo = await si.networkInterfaces();
  const data = JSON.stringify(networkInfo)
  registerData(category, data)
}

module.exports = networkData;
