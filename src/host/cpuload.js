const si = require("systeminformation");
const registerData = require("../../core/registerData")

const category = "cpuload"

async function cpuLoadData() {
    const cpuLoadInfo = await si.currentLoad();
    const data = JSON.stringify(cpuLoadInfo)
    registerData(category, data)
}

module.exports = cpuLoadData;
