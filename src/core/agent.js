const req = require("request")
const cfg = require("../config/cfg")

const module_ords = "/register/alive_agent"

function registerAgentAlive() {
  var options = {
    headers: { "content-type": "application/x-www-form-urlencoded" },
    url: cfg.endpoint + module_ords,
    form: {
      secret_key: cfg.secret_key
    }
  };
  req.post(options, (err, req) => {
    if (!err && req.statusCode === 200) {
      console.log(`[ Agent ] - Alive request endpoint with success`)
    } else {
      if (!err) {
        console.log(`Request endpoint return status code: ${req.statusCode}`)
      } else {
        console.log(err)
      }
    }
  });
}

module.exports = registerAgentAlive