const datetime = require("node-datetime")
const req = require("request")
const fs = require("fs")
const cfg = require("../../config/cfg")

function definitionLevel(level) {
  switch (level) {
    case 1:
      return 'INFO'
    case 2:
      return 'CRITICAL'
    default:
      return 'UNKOWN'
  }
}

function currentDateTime() {
  let dt = datetime.create()
  let dateTimeFormated = dt.format(cfg.patterns.date_format)
  return dateTimeFormated
}

function logRegister(level, category, msg) {
  let levelStored = definitionLevel(level)
  let dt = currentDateTime()
  out = `[${levelStored}] [${category}] - ${dt} - ${msg}\n\n`
  fs.appendFile(cfg.paths.log, msg, err => {
    if (err) {
      console.log(err)
    } else {
      console.log("log registred with success!")
    }
  })
}

function registerDataEndpoint(moduleRequest, data) {
  try {
    let options = {
      headers: { "content-type": "application/x-www-form-urlencoded" },
      url: cfg.api.endpoint + moduleRequest,
      form: {
        par_param_log: data,
        secret_key: cfg.api.secret_key
      }
    }
    req.post(options, (err, req) => {
      if (!err && req.statusCode === 200) {
        logRegister(1, category, `Transmission to endpoint ${options.url} with success!`)
      } else {
        logRegister(2, category, `Transmission to endpoint ${options.url} failed!`)
        logRegister(2, category, err)
      }
    })
  } catch (err) {
    logRegister(2, category, `Try failed to transmission endpoint ${options.url}!`)
  }
}

module.exports = {
  logRegister, registerDataEndpoint
}