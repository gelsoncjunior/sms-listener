var net = require("net");

var ping = function (options, callback) {
  var i = 0;
  var results = [];
  options.address = options.address;
  options.port = options.port;
  options.attempts = options.attempts || 3;
  options.timeout = options.timeout;

  function statusReturn(avg) {
    if (avg > 0 && avg < 300) {
      return status = 1;
    } else if (avg > 300) {
      return status = 2;
    } else if (avg == 0) {
      return status = 3;
    } else {
      return status = 4;
    }
  }

  var check = function (options, callback) {
    if (i < options.attempts) {
      connect(
        options,
        callback
      );
    } else {

      var avg = results.reduce(function (prev, curr) {
        return prev + curr.time;
      }, 0);
      var max = results.reduce(function (prev, curr) {
        return prev > curr.time ? prev : curr.time;
      }, results[0].time);
      var min = results.reduce(function (prev, curr) {
        return prev < curr.time ? prev : curr.time;
      }, results[0].time);

      avg = (avg / results.length);

      statusReturn(avg);

      var out = {
        address: options.address,
        port: options.port,
        attempts: options.attempts,
        avg: avg,
        max: max,
        min: min,
        status: status
        //results: results
      };
      callback(undefined, out);
    }
  };

  var connect = function (options, callback) {
    var s = new net.Socket();
    var start = process.hrtime();
    s.connect(options.port, options.address, function () {
      var time_arr = process.hrtime(start);
      var time = (time_arr[0] * 1e9 + time_arr[1]) / 1e6;
      results.push({ seq: i, time: time });
      s.destroy();
      i++;
      check(options, callback);
    });
    s.on("error", function (e) {
      results.push({ seq: i, time: 0, err: e });
      s.destroy();
      i++;
      check(options, callback);
    });
    s.setTimeout(1000, function () {
      results.push({ seq: i, time: 0, err: Error("Request timeout") });
      s.destroy();
      i++;
      check(options, callback);
    });
  };
  connect(
    options,
    callback
  );
};

module.exports.ping = ping;