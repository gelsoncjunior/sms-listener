const patterns = {
  date_format: "d/m/Y H:M:S",
}

const api = {
  secret_key: "d92230bdaf32c52ef725e51f0999fdef",
  endpoint: "http://portal.sms-analysis.me:8001",
  modules: {
    registerParamLog: "/register/param_log",
    registerAgent: "/register/agent"
  }
}

const paths = {
  log: "logs/sms.out",
  dataJson: ""
}

module.exports = {
  cfg, paths, api, patterns
}